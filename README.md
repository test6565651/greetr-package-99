Hi, I am Rifat Hossain.
Email: rh140025@gmail.com

This is a simple Laravel Crud package.
To use this package, Please Follow the following steps:

1.In your composer.json file, add repositoris using the fiollowing format.

`"repositories": [ { "type": "vcs", "url": "https://gitlab.com/test6565651/greetr-package-99" } ],`

2.in your terminal run the following command:
`$composer update`


3.to install the package run the following command:
`$composer require simplexi/greetr`


4.add the path to providers in config/app.php:
`\Simplexi\Greetr\CrudServiceProvider::class,`


5.to create complete CURD files (Model, View, Controller, Request, Migrations) and source code, Run the floowing command:
`$php artisan simplexi:crud {name} {columns}`

Example:
`$php artisan simplexi:crud BlogPost title:string,description:text,status:tinyInteger`

If Any error occurs please  do consider the evaluate the code.
Thank You.
