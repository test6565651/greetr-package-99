<?php

namespace Simplexi\Greetr;

use Illuminate\Support\ServiceProvider;

class CrudServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->commands([
            Commands\CrudCommand::class,
        ]);
    }
}
