<?php

namespace Simplexi\Greetr;

class Greetr
{
    protected $modelName;

    public function __construct($modelName)
    {
        $this->modelName = $modelName;
        $this->generateModel();
    }

    protected function generateModel()
    {
        $stub = file_get_contents(__DIR__ . '/stubs/model.stub');
        $modelContent = str_replace('{{modelName}}', $this->modelName, $stub);
        file_put_contents(app_path("Models/{$this->modelName}.php"), $modelContent);
    }
}
