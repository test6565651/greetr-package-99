<?php

namespace Simplexi\Greetr\Services;

class ControllerMakerService
{
    private $modelName;
    private $columns;

    public function __construct($modelName, $columns)
    {
        $this->modelName = $modelName;
        $this->columns = $columns;

        $this->generateController();
    }

    private function generateController()
    {
        $ModelName = ucfirst($this->modelName);
        $modelName = strtolower($this->modelName);

        // get stub and replace model name
        $stub = file_get_contents(__DIR__ . '/../stubs/controller.stub');
        $controllerContent = str_replace('{{ModelName}}', $ModelName, $stub);
        $controllerContent = str_replace('{{modelName}}', $modelName, $controllerContent);

        file_put_contents(app_path("Http/Controllers/{$ModelName}Controller.php"), $controllerContent);
    }
}
