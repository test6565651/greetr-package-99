<?php

namespace Simplexi\Greetr\Services;

use Illuminate\Support\Str;

class MigrationMakerService
{
    private $modelName;
    private $columns;

    public function __construct($modelName, $columns)
    {
        $this->modelName = $modelName;
        $this->columns = $columns;

        $this->generateMigration();
    }

    private function generateMigration()
    {
        $tableName = strtolower($this->modelName);
        $columns = explode(',', $this->columns);


        //explode words by Capital letters
        $words = preg_split('/(?=[A-Z])/', $this->modelName, -1, PREG_SPLIT_NO_EMPTY);
        $name = '';
        foreach ($words as $word) {
            //make the last word plural
            if ($word == end($words)) {
                $word = Str::plural($word);
            }
            $name .= strtolower($word) . '_';
        }
        //remove last underscore
        $name = substr($name, 0, -1);



        // get stub and replace model name
        $stub = file_get_contents(__DIR__ . '/../stubs/migration.stub');
        $migrationContent = str_replace('{{tableName}}', $name, $stub);

        // prepare columns
        $table_columns = '';
        foreach ($columns as $column) {
            $data = explode(':', $column);

            $table_columns .= "\$table->" . trim($data[1] ?? 'string') . "('" . trim($data[0]) . "')->nullable();\n\t\t\t";
        }

        // removing spaces
        $table_columns = substr($table_columns, 0, -4);

        $migrationContent = str_replace('{{columns}}', $table_columns, $migrationContent);

        //check if migrations folder exists
        if (!file_exists(database_path('migrations'))) {
            mkdir(database_path('migrations'));
        }

        file_put_contents(database_path("migrations/" . date('Y_m_d_His') . "_create_{$name}_table.php"), $migrationContent);
    }
}
