<?php

namespace Simplexi\Greetr\Services;

class ModelMakerService
{
    private $modelName;
    private $columns;

    public function __construct($modelName, $columns)
    {
        $this->modelName = $modelName;
        $this->columns = $columns;

        $this->generateModel();
    }

    private function generateModel()
    {
        $ModelName = ucfirst($this->modelName);
        $modelName = strtolower($this->modelName);
        $columns = explode(',', $this->columns);

        // get stub and replace model name
        $stub = file_get_contents(__DIR__ . '/../stubs/model.stub');
        $modelContent = str_replace('{{ModelName}}', $ModelName, $stub);
        $modelContent = str_replace('{{modelName}}', $modelName, $modelContent);

        // prepare data
        $fillable = '';
        foreach ($columns as $column) {
            $data = explode(':', $column);
            $fillable .= "'" . trim($data[0]) . "',\n\t\t";
        }
        // removing last comma
        $fillable = substr($fillable, 0, -4);
        $modelContent = str_replace('{{fillable_columns}}', $fillable, $modelContent);

        // prepare data
        $prepareData = '';
        foreach ($columns as $column) {
            $data = explode(':', $column);
            $prepareData .= "'" . trim($data[0]) . "' => $" . "request->input('" . trim($data[0]) . "'),\n\t\t\t";
        }
        // removing spaces
        $prepareData = substr($prepareData, 0, -4);
        $modelContent = str_replace('{{prepare_data}}', $prepareData, $modelContent);

        file_put_contents(app_path("Models/{$ModelName}.php"), $modelContent);
    }
}
