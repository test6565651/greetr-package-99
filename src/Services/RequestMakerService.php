<?php

namespace Simplexi\Greetr\Services;

class RequestMakerService
{
    private $modelName;
    private $columns;

    public function __construct($modelName, $columns)
    {
        $this->modelName = $modelName;
        $this->columns = $columns;

        $this->generateStoreRequest();
        $this->generateUpdateRequest();
    }

    private function generateStoreRequest()
    {
        $ModelName = ucfirst($this->modelName);
        $modelName = strtolower($this->modelName);
        $columns = explode(',', $this->columns);

        // get stub and replace model name
        $stub = file_get_contents(__DIR__ . '/../stubs/request.stub');
        $requestContent = str_replace('{{RequestClass}}', 'Store' . $ModelName . 'Request', $stub);

        // prepare rules
        $rules = '';
        foreach ($columns as $column) {
            $data = explode(':', $column);
            $rules .= "'" . trim($data[0]) . "' => 'nullable',\n\t\t\t";
        }

        // removing spaces
        $rules = substr($rules, 0, -4);

        $requestContent = str_replace('{{rules}}', $rules, $requestContent);

        //check if Request folder exists
        if (!file_exists(app_path('Http/Requests'))) {
            mkdir(app_path('Http/Requests'));
        }

        file_put_contents(app_path("Http/Requests/Store{$ModelName}Request.php"), $requestContent);
    }

    private function generateUpdateRequest()
    {
        $ModelName = ucfirst($this->modelName);
        $modelName = strtolower($this->modelName);
        $columns = explode(',', $this->columns);

        // get stub and replace model name
        $stub = file_get_contents(__DIR__ . '/../stubs/request.stub');
        $requestContent = str_replace('{{RequestClass}}', 'Update' . $ModelName . 'Request', $stub);

        // prepare rules
        $rules = '';
        foreach ($columns as $column) {
            $data = explode(':', $column);
            $rules .= "'" . trim($data[0]) . "' => 'nullable',\n\t\t\t";
        }

        // removing spaces
        $rules = substr($rules, 0, -4);

        $requestContent = str_replace('{{rules}}', $rules, $requestContent);

        file_put_contents(app_path("Http/Requests/Update{$ModelName}Request.php"), $requestContent);
    }
}
