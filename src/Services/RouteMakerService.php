<?php

namespace Simplexi\Greetr\Services;

use Illuminate\Support\Str;

class RouteMakerService
{
    private $modelName;
    private $columns;

    public function __construct($modelName, $columns)
    {
        $this->modelName = $modelName;
        $this->columns = $columns;

        $this->generateRoute();
    }

    private function generateRoute()
    {
        $ModelName = ucfirst($this->modelName);
        $modelName = strtolower($this->modelName);

        //explode words by Capital letters
        $words = preg_split('/(?=[A-Z])/', $this->modelName, -1, PREG_SPLIT_NO_EMPTY);
        $route_name = '';
        foreach ($words as $word) {
            //make the last word plural
            if ($word == end($words)) {
                $word = Str::plural($word);
            }
            $route_name .= strtolower($word) . '-';
        }
        //remove last underscore
        $route_name = substr($route_name, 0, -1);

        // get stub and replace model name
        $stub = file_get_contents(__DIR__ . '/../stubs/route.stub');
        $routeContent = str_replace('{{ModelName}}', $ModelName, $stub);
        $routeContent = str_replace('{{modelName}}', $modelName, $routeContent);
        $routeContent = str_replace('{{route}}', $route_name, $routeContent);

        file_put_contents(base_path("routes/web.php"), $routeContent, FILE_APPEND);


        $controllerContent = "\nuse App\Http\Controllers\\{$ModelName}Controller;";

        $file = base_path("routes/web.php");
        $contents = file_get_contents($file);
        $contents = str_replace('<?php', "<?php\n\n" . $controllerContent, $contents);

        file_put_contents($file, $contents);
    }
}
