<?php

namespace Simplexi\Greetr\Services;

use Illuminate\Support\Str;

class ViewMakerService
{
    private $modelName;
    private $columns;

    public function __construct($modelName, $columns)
    {
        $this->modelName = $modelName;
        $this->columns = $columns;

        $this->generateIndex();
        $this->generateCreate();
        $this->generateEdit();
    }

    private function generateIndex()
    {
        $ModelName = ucfirst($this->modelName);
        $modelName = strtolower($this->modelName);
        $columns = explode(',', $this->columns);

        // get stub and replace model name
        $stub = file_get_contents(__DIR__ . '/../stubs/view/layout.stub');

        //check resiurces/views/layouts/crud folder exists
        if (!file_exists(resource_path('views/layouts/crud'))) {
            mkdir(resource_path('views/layouts/crud'), 0777, true);
        }

        //creating layout file 
        file_put_contents(resource_path("views/layouts/crud/layout.blade.php"), $stub);


        // get stub and replace model name
        $stub = file_get_contents(__DIR__ . '/../stubs/view/index.stub');
        $viewContent = str_replace('%%modelName%%', $modelName, $stub);

        //prepare column
        $table_columns = '';
        foreach ($columns as $column) {
            $data = explode(':', $column);

            $table_columns .= "<th>" . trim($data[0]) . "</th>\n\t\t\t";
        }
        // removing spaces
        $table_columns = substr($table_columns, 0, -4);

        $viewContent = str_replace('%%column%%', $table_columns, $viewContent);

        //prepare data
        $table_data = '';
        foreach ($columns as $column) {
            $data = explode(':', $column);
            $table_data .= "<td>{{ $" . "item->" . trim($data[0]) . " }}</td>\n\t\t\t";
        }
        // removing spaces
        $table_data = substr($table_data, 0, -4);

        $viewContent = str_replace('%%data%%', $table_data, $viewContent);

        //check resiurces/views/$modelName folder exists
        if (!file_exists(resource_path("views/{$modelName}"))) {
            mkdir(resource_path("views/{$modelName}"), 0777, true);
        }

        file_put_contents(resource_path("views/{$modelName}/index.blade.php"), $viewContent);
    }

    private function generateCreate()
    {
        $ModelName = ucfirst($this->modelName);
        $modelName = strtolower($this->modelName);
        $columns = explode(',', $this->columns);

        // get stub and replace model name
        $stub = file_get_contents(__DIR__ . '/../stubs/view/create.stub');
        $viewContent = str_replace('%%modelName%%', $modelName, $stub);

        //prepare column
        $form_fields = '';
        foreach ($columns as $column) {
            $data = explode(':', $column);

            $form_fields .= "<label for=\"" . trim($data[0]) . "\">" . trim($data[0]) . "</label>\n<input type=\"text\" name=\"" . trim($data[0]) . "\" id=\"" . trim($data[0]) . "\" class=\"form-control\" placeholder=\"" . trim($data[0]) . "\">@error('" . trim($data[0]) . "')\n<div class=\"alert alert-danger\">{{ $" . "message }}</div>\n@enderror\n";
        }
        // removing spaces
        // $form_fields = substr($form_fields, 0, -4);

        $viewContent = str_replace('%%formFields%%', $form_fields, $viewContent);

        //check resiurces/views/$modelName folder exists
        if (!file_exists(resource_path("views/{$modelName}"))) {
            mkdir(resource_path("views/{$modelName}"), 0777, true);
        }

        file_put_contents(resource_path("views/{$modelName}/create.blade.php"), $viewContent);
    }

    private function generateEdit()
    {
        $ModelName = ucfirst($this->modelName);
        $modelName = strtolower($this->modelName);
        $columns = explode(',', $this->columns);

        // get stub and replace model name
        $stub = file_get_contents(__DIR__ . '/../stubs/view/edit.stub');
        $viewContent = str_replace('%%modelName%%', $modelName, $stub);

        //prepare column
        $form_fields = '';
        foreach ($columns as $column) {
            $data = explode(':', $column);

            $form_fields .= "<label for=\"" . trim($data[0]) . "\">" . trim($data[0]) . "</label>\n<input type=\"text\" name=\"" . trim($data[0]) . "\" id=\"" . trim($data[0]) . "\" class=\"form-control\" placeholder=\"" . trim($data[0]) . "\" value=\"{{ $" . $modelName ."->" . trim($data[0]) . " }}\">@error('" . trim($data[0]) . "')\n<div class=\"alert alert-danger\">{{ $" . "message }}</div>\n@enderror\n";
        }
        // removing spaces
        // $form_fields = substr($form_fields, 0, -4);

        $viewContent = str_replace('%%formFields%%', $form_fields, $viewContent);

        //check resiurces/views/$modelName folder exists
        if (!file_exists(resource_path("views/{$modelName}"))) {
            mkdir(resource_path("views/{$modelName}"), 0777, true);
        }

        file_put_contents(resource_path("views/{$modelName}/edit.blade.php"), $viewContent);
    }
}
